(function () {'use strict';

/**
 * Created by nneubaue on 09/11/15.
 */

function jQuery() {
    var jQuery = require('./bower_components/jquery/dist/jquery.js');
    return jQuery;
}

var Renderer = function (options, d3) {
    var
        self = this;

    self.options = options;
    self.force = d3.layout.force()
        .charge(-1500)
        .linkDistance(30)
        .size([options.width, options.height]);

    self.svg = d3.select("#svg_graph")
        .attr("width", options.width)
        .attr("height", options.height);

};

Renderer.prototype.resize = function(width, height) {
    var
        self = this;
    self.svg
        .attr("width", width)
        .attr("height", height);

    self.force.size([width, height]);
};

Renderer.prototype.clear = function() {
    "use strict";
    this.render({nodes:[], edges:[]});
};


Renderer.prototype.render = function(levelState) {
    var
        self = this;

    self.svg.selectAll("*").remove();

    self.nodes = levelState.level.nodes.map(function(node) { return JSON.parse(JSON.stringify(node));})
    self.edges = levelState.level.edges.map(function(edge) { return JSON.parse(JSON.stringify(edge));})

    // convert from indices of nodes to references to nodes
    angular.forEach(self.edges, function(edge) {
        edge.source = self.nodes[edge.source];
        edge.target = self.nodes[edge.target];
    })

    self.force
        .nodes(self.nodes)
        .links(self.edges)
        .start();


    self.force.on("tick", function() {
        self.link.attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

        self.node.attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });
    });

    self.needsForceStart = true;
    self.lastNodes = self.nodes.length;

    self.update(levelState);
};

// TODO zoom
// http://bl.ocks.org/mbostock/4699541
// http://jsfiddle.net/56RDx/2/
// http://bl.ocks.org/mbostock/6123708

Renderer.prototype.update = function(levelState) {
    var
        self = this,
        state = levelState.state;



    var showNodes = self.nodes.filter(function (node) {
        // return true;
        if (!state)
            return true;
        return state[node.id] === undefined || state[node.id].state === undefined || state[node.id].state >-1;
    });

    if (showNodes.length != self.lastNodes) {
        self.lastNodes = showNodes.length;
        self.needsForceStart = true;
    }


    var nodeIndices = {};
    for (var i=0; i<showNodes.length; i++) {
        nodeIndices[showNodes[i].id] = i; // mapping old index to new index
    }

    var showEdges = self.edges.filter(function (edge) {
        return (nodeIndices[edge.source.id] !== undefined && nodeIndices[edge.target.id] !== undefined);
    });

    self.link = self.svg.selectAll(".link")
        .data(showEdges);

    self.link.enter().append("line")
        .attr("class", "link")
        .style("stroke-width", 2); // (function(d) { return Math.sqrt(d.value); });

    self.link.exit().remove();



    self.node = self.svg.selectAll(".node")
        .data(showNodes, function(node) { return node.id; });

    self.node
        .enter().append("circle")
        .attr("class", "node")
        .attr("r", 10)
        .style("stroke-width", 5)
        .style("stroke", function(d) { return self.options.color(d.targetState); })
        .call(self.force.drag)
        .on("click", function(d) {
            self.onNodeClick(d);
        })
        .append("title")
        .text(function(d) { return d.name; });

    self.node
        .style("fill", function(d)  {
            if (!state)
                return "#FFF";
            if (!state[d.id])
                return "#FFF";
            if (state[d.id].state === undefined)
                return "#FFF";
            return self.options.color(state[d.id].state);
        }
    );

    self.node.exit().remove();

    if (self.needsForceStart) {
        self.force
            .nodes(showNodes)
            .links(showEdges)
            .start();
        self.needsForceStart = false;
    }
};

Renderer.prototype.onNodeClick = function(o) {
    var self = this;
    if (self._onNodeClick)
        self._onNodeClick(o);
}

Renderer.prototype.setOnNodeClick = function(fn) {
    var self = this;
    self._onNodeClick = fn;
}

/**
 * Created by nneubaue on 08/11/15.
 */
// static state which needs to be loaded from JSON
var GameData = function($http) {
    this.data = {
        levels: []
    };
    this.$http = $http;
};

GameData.prototype.loadLevels = function(url, callback) {
    var self = this;
    var $http = self.$http;

    // TODO store in var and to $q.all, rename from loadLevels to loadAll
    $http.get(url).then(function(response) {
        self.data.levels = response.data.levels;
        angular.forEach(self.data.levels, function (level, index) {
            level.id = index;
        });
    });
    callback();
};

GameData.prototype.getNumLevels = function() {
    return this.data.levels.length;
};

/**
 * Created by nneubaue on 08/11/15.
 */
// overall game state: which levels are visible
class GameState {
    constructor(gameData, soundEngine) {
        console.log(gameData, soundEngine);

        this.activeLevel = null;
        this.levelsState = {};
        this.gameData = gameData;
        this.initialized = false;
        this.soundEngine = soundEngine;
    }

    init () {
        this.levelsState = {};

        for (let i = 0; i < this.gameData.getNumLevels(); i++) {
            this.levelsState[i] = {
                visible: i === 0, //  true, //i == 0,
                score: 0,
                won: false
            };
        }
        this.initialized = true;
    }

    unlockNextLevel(index) {
        const numLevels = this.gameData.getNumLevels();

        if (index >= numLevels - 1) {
            return false;
        }
        if (this.levelsState[index + 1] === undefined) {
            this.levelsState[index + 1] = {};
        }

        this.levelsState[index + 1].visible = true; // TODO make visible to binding thingie
    }
}

/**
 * Created by nneubaue on 08/11/15.
 */
// state of an individual level
var LevelState = function (renderer, networkLogic, soundEngine) {
    this.renderer = renderer;
    this.networkLogic = networkLogic;
    this.soundEngine = soundEngine;
    this.delay = 1500;
};

LevelState.prototype.initLevel = function initLevel(level) {
    this.level = level; // JSON.parse(JSON.stringify(level)); // deep copy, we want to be able to manipulate stuff without worrying about changing the game
    // store index because we will want to remove elements
    angular.forEach(this.level.nodes, function(node, index) {
        node.id = index; // have to call this id because index is overwritten by D3
    });
    angular.forEach(this.level.edges, function(edge, index) {
        edge.id = index;
    });
    this.state = {};
    this.moves = [];
    var states = {};
    var count = 0;
    angular.forEach(level.moves, function(moveType) {
        for(var i=0; i<moveType.count; i++) {
            this.moves.push(moveType);
            moveType.index = count++;
            states[moveType.state] = 1;
        }
    }, this);
    this.possibleStates = Object.keys(states);
    this.numStates = this.possibleStates.length;
};

LevelState.prototype.checkTargetState = function checkTargetState() {
    var
        correct = 0,
        incorrect = 0,
        unset = 0,
        exploded = 0,
        self = this;

    angular.forEach(self.level.nodes, function(node) {
        if (self.state[node.id] === undefined) {
            unset += 1;
        } else if (self.state[node.id].state == -1) {
            exploded += 1;
        } else if (self.state[node.id].state == node.targetState) {
            correct += 1;
        } else {
            incorrect += 1;
        }
    });

    return {
        correct: correct,
        incorrect: incorrect,
        unset: unset,
        exploded: exploded,
        won: incorrect + unset == 0
    }
};


LevelState.prototype.getState = function getState(node) {
    var
        self = this;
    if (!self.state[node.id])
        self.state[node.id] = {};
    return self.state[node.id].state;
};

LevelState.prototype.setState = function setState(node, state) {
    var
        self = this;
    if (!self.state[node.id])
        self.state[node.id] = {};
    self.state[node.id].state = state;
};

LevelState.prototype.oneStep = function oneStep(previousNodes, step, inGame, delay, callback) {
    var
        changedNodes = {},
        numchangedNodes = 0,
        newPrevious = {},
        self = this;

    ///console.log('state', self.state);

    // console.log("previousNodes", previousNodes);

    angular.forEach(previousNodes, function(nodestate, nodeIndex) {
        //console.log('edges', self.level.edges);
        var neighbors = self.networkLogic.getNeighbors(nodeIndex, self.level.edges, 1);
        //console.log("neighbors for node ", nodeIndex, neighbors, ", node has state", nodestate);

        angular.forEach(neighbors, function(depth, nodeIndex2) {
            if (self.state[nodeIndex2]) {
                if (self.state[nodeIndex2].state !== undefined) {
                    // console.log("node has state", node2, self.state[node2]);
                    return;
                }
            }
            if (!changedNodes[nodeIndex2])
                changedNodes[nodeIndex2] = [];
            // TODO more efficient
            if (changedNodes[nodeIndex2].indexOf(nodestate) === -1)
                changedNodes[nodeIndex2].push(nodestate);
            // console.log("node now has state", node2, changedNodes[node2]);
        });
    });

    if (inGame)
        self.soundEngine.playMove(changedNodes, step, this.numStates, delay);

    angular.forEach(changedNodes, function(states, nodeIndex) {
        if (!self.state[nodeIndex])
            self.state[nodeIndex] = {};

        if (states.length == 1) {
            self.state[nodeIndex] = states[0];
            newPrevious[nodeIndex] = states[0];
            numchangedNodes += 1;
        } else {
            var firstState = states[0].state;
            self.state[nodeIndex].state = firstState;
            for (var i=1; i < states.length; i++) {
                if (states[i].state !== firstState) {
                    self.state[nodeIndex].state = -1;
                    break;
                }
            }
        }
    });


    //console.log("vorher", self.state, JSON.stringify(self.level.nodes.map(function(x) { return [x.index, self.state[x.index]];})));
    if (inGame)
        self.renderer.update(self);
    //console.log("nachher", self.level.nodes.map(function(x) { return x.index;}));

    if (numchangedNodes>0) {
        //console.log('changedNodes', numchangedNodes, newPrevious);
        if (inGame)
            setTimeout(function() {self.oneStep(newPrevious, step+1, inGame, delay, callback);}, delay);
        else
            self.oneStep(newPrevious, step+1, inGame, delay, callback);
    }
    else {
        //console.log('final state', self.state);
        callback();
    }
};

LevelState.prototype.play = function play(callback) {
    var self = this;
    this.oneStep(self.state, 0, true, self.delay, callback);
};

/**
 * Created by nneubaue on 15/11/15.
 */
function NetworkLogic() {

}

NetworkLogic.prototype.getNeighbors = function getNeighbors(id, edges, depth) {
    var nodes = {};
    nodes[id] = 0;
    this.extendNodes(nodes, edges, 1, depth);
    return nodes;
};

NetworkLogic.prototype.extendNodes = function(nodes, edges, depth, targetDepth) {
    // TODO: more efficient
    angular.forEach(nodes, function(nodeDepth, node) {
        angular.forEach(edges, function(edge, index) {
            if (edge.target == node) {
                if (!nodes[edge.source])
                    nodes[edge.source] = depth;
            } else if (edge.source == node) {
                if (!nodes[edge.target])
                    nodes[edge.target] = depth;
            }
        });
    });
    if (depth<targetDepth)
        extendNodes(nodes, edges, depth+1, targetDepth);
};

const angular$2 = window.angular;
const scales = [
    ['C3', 'D3', 'E3', 'F3', 'G3'],
    ['E3', 'F3', 'G3', 'A3', 'B3'],
    ['G3', 'B3', 'D4', 'C4', 'E4']

];

const makeInstruments = function(Wad) {
    const getNote = (_source, _pitch, _attack, _decay, _sustain, _hold, _release, _delay) =>
        new Wad({
            source: _source,
            pitch: _pitch,
            volume: 0.3,
            env: {
                attack: _attack,
                decay: _decay,
                sustain: _sustain,
                hold: _hold,
                release: _release
            },
            delay: {
                wet: 1,
                delayTime: _delay,
                feedback: 0.25
            },
            tuna: {
                Chorus: {
                    intensity: 0.3,  // 0 to 1
                    rate: 4,         // 0.001 to 8
                    stereoPhase: 0, // 0 to 180
                    bypass: 0
                }
            }
        });

    const niceSound = (note, count, duration, delay) =>
        getNote('sine',
            note, duration * 0.1,
            duration * 0.1,
            duration * 1,
            duration * 0.1,
            duration * 1,
            count > 1 ? duration / count : 0);

    const errorSound = () =>
        getNote('sawtooth', 'F4', 0, 0.1, 0, 0, 0, 0);

    return {
        colors: [
            niceSound,
            niceSound,
            niceSound
        ],
        noMove: errorSound
    };
};

class SoundEngine {
    constructor(Wad, howler) {
        this.scales = scales;
        this.instruments = makeInstruments(Wad);
        this.Wad = Wad;
        this.howler = howler;
    }

    getPureStateDelay(state, duration, numStates) {
        return state * (duration / numStates);
    }

    // TODO know right from wrong moves
    pureStateSound(state, index, count, duration, delay) {
        const instrument = this.instruments.colors[state];
        const note = this.scales[state][index % this.scales[state].length];
        return instrument(note, count, duration, delay);
    };

    playNoMoveSelected () {
        return this.instruments.noMove().play();
    }

    playSingleMove(state, duration) {
        return this.pureStateSound(
            state,
            0,
            1,
            duration / 1000,
            duration / 1000).play();
    };

    /* moves = {node id: [{state: count}]} */
    playMove(moves, index, numStates, duration) {
        const pureStates = {};
        const crashes = {};
        angular$2.forEach(moves, (states, _nodeId) => {
            if (states.length === 1) {
                var state = states[0].state;
                if (state === undefined) {
                    return;
                }
                if (pureStates[state]) {
                    pureStates[state] += 1;
                } else {
                    pureStates[state] = 1;
                }
            } else {
                const crashIndex = ''; // TODO
                if (crashes[crashIndex]) {
                    crashes[crashIndex] += 1;
                } else {
                    crashes[crashIndex] = 1;
                }
            }
        });

        const poly = new this.Wad.Poly();
        angular$2.forEach(pureStates, function(count, state) {
            poly.add(this.pureStateSound(
                state,
                index,
                count,
                duration / 1000,
                duration / (1000 * numStates)));
        }, this);
        poly.play();

        // TODO crashSounds
        // TODO distinguish between right and wrong assignment!
        // TODO click sound
    }

    playMusic(occasion) {
        switch (occasion) {
        case 'lounge':
            this.music = new this.howler.Howl({
                src: ['./sound/Groove_Loop.wav'],
                loop: true
            });
            this.music.play();
            break;
        default:
            console.error('unknown occasion ' + occasion);
        };
    }
}

/**
 * Created by nneubaue on 09/11/15.
 */

// main controller, wraps everything
function MainCtrl($scope, gameData, gameState, soundEngine) {
    $scope.name = 'me';
    $scope.ready = false;
    $scope.gameData = gameData;
    $scope.gameState = gameState;
    this.soundEngine = soundEngine;

    gameData.loadLevels('data/levels.json', () => {
        $scope.ready = true;
        this.soundEngine.playMusic('lounge');
    });

    $scope.newGame = () => {
        $scope.gameState.init();
    };

    $scope.showStartScreen = () => !$scope.gameState.initialized;

    $scope.showGameScreen = () => $scope.gameState.initialized;
}

/**
 * Created by nneubaue on 09/11/15.
 */
// controller for UI above single level, i.e. which levels are visible etc

class GameCtrl {
    constructor ($scope, gameData, gameState) {
        this.$scope = $scope;
        $scope.gameState = gameState;
        $scope.levelsState = gameState.levelsState;

        $scope.runLevel = levelIndex => {
            $scope.gameState.activeLevelIndex = levelIndex;
            $scope.gameState.activeLevel = gameData.data.levels[levelIndex];
            $scope.$broadcast('startLevel', {
                level: $scope.gameState.activeLevel,
                levelIndex: levelIndex
            });
        };

        $scope.getNumLevels = () => gameData.getNumLevels();

        $scope.$on('levelWon', (event, levelIndex) => {
            this.levelWon(levelIndex);
        });
    }

    levelWon (levelIndex) {
        this.$scope.won = true;
        this.$scope.gameState.unlockNextLevel(levelIndex);
        this.$scope.$apply();
    }
}

/**
 * Created by nneubaue on 09/11/15.
 */
// controls everything for the actual level being played
function LevelCtrl($scope, gameState, levelState, renderer, soundEngine) {
    var self = this;
    self.levelState = levelState;
    self.$scope = $scope;
    $scope.levelState = levelState;
    $scope.won = false;
    $scope.selectedMove = null;
    $scope.selectedMoveIndex = -1;
    self.soundEngine = soundEngine;

    $scope.play = function() {
        self.levelState.play(function() {
            var ts = levelState.checkTargetState();
            console.log('targetState:', ts);
            if (ts.won) {
                self.won();
            }
        })
    };

    this.gameState = gameState;
    this.levelState = levelState;
    this.renderer = renderer;
    $scope.$on('startLevel', function(event, arg) {
        self.initLevel(arg.level, arg.levelIndex);
        $scope.levelState = levelState;
    });
    renderer.setOnNodeClick(function(d) {self.nodeClick(d);}); // save this

    $scope.getMoveClasses = function(move) {
        var s = 'control state' + move.state;
        if (move.index===this.selectedMoveIndex) {
            s += ' selectedMove';
        }
        //'control state'+{{move.state}} + ({{move.index}}=={{selectedMoveIndex}}?' selectedMove':'')
        return s;
    }

    $scope.selectMove = self.selectMove;
}

LevelCtrl.prototype.initLevel = function initLevel(level, levelIndex) {
    var
        self = this;
    self.$scope.won = false;
    self.levelState.initLevel(level);
    self.levelIndex = levelIndex;
    self.$scope.moves = self.levelState.moves;
    self.renderer.render(self.levelState);

};

LevelCtrl.prototype.selectMove = function selectMove(move) {
    if (this.$parent.selectedMove === move) {
        this.$parent.selectedMove = undefined;
        this.$parent.selectedMoveIndex = -1;
    } else {
        this.$parent.selectedMove = move;
        this.$parent.selectedMoveIndex = move.index;
    }
}

LevelCtrl.prototype.won = function won() {
    var self = this;
    self.$scope.$emit("levelWon", self.levelIndex);
}

LevelCtrl.prototype.deleteMove = function deleteMove(move) {
    var moves = this.$scope.moves;
    var index = moves.indexOf(move);
    moves.splice(index, 1);
}

LevelCtrl.prototype.nodeClick = function nodeClick(node) {
    var
        self = this,
        currentState = self.levelState.getState(node);

    console.log(node, currentState);

    if (!self.$scope.selectedMove) {
        console.log("no target state selected!");
        self.soundEngine.playNoMoveSelected();
        return;
    }

    if (currentState !== undefined)
        return false; // state's already set, nothing to do here

    self.levelState.setState(node, self.$scope.selectedMove.state);
    self.soundEngine.playSingleMove(self.$scope.selectedMove.state, 1500); // TODO get 750 from somewhere central


    self.renderer.update(self.levelState);

    self.deleteMove(self.$scope.selectedMove);
    self.$scope.selectedMove = undefined;
    self.$scope.selectedMoveIndex = -1;


    var ts = self.levelState.checkTargetState();

    if (ts.won) {
        self.won();
    }

    self.$scope.$apply();

    return true;
};

const angular$3 = window.angular;

const pickN = function pickN(a, n, callback) {
    const l = a.length;
    if (n === 0) {
        return [];
    }
    if (n === l) {
        if (callback) {
            callback(a);
        }
        return [a];
    }
    var indices = Array(n);
    for (let i = 0; i < n; i++) {
        indices[i] = i;
    }

    function increase(rindex) {
        const index = n - 1 - rindex;
        indices[index] += 1;
        if (indices[index] === l - rindex) {
            if (index > 0) {
                increase(rindex + 1);
                indices[index] = indices[index - 1] + 1;
            }
        }
    }

    var results = [];
    while (indices[0] <= l - n) {
        var pick = indices.map(function(index) {
            return a[index];
        });
        if (callback) {
            callback(pick);
        } else {
            results.push(pick);
        }
        increase(0);
    }

    return results;
};

const permutator = function permutator(inputArr) {
    var results = [];

    function permute(arr, memo) {
        var cur;
        memo = memo || [];

        for (var i = 0; i < arr.length; i++) {
            cur = arr.splice(i, 1);
            if (arr.length === 0) {
                results.push(memo.concat(cur));
            }
            permute(arr.slice(), memo.concat(cur));
            arr.splice(i, 0, cur[0]);
        }

        return results;
    }

    return permute(inputArr);
};

const forEachConfig = function forEachConfig(moves, nodes, callback) {
    var subnodes = null;
    var submoves;
    var evaluatedPerms = {};
    var foundPerfect = false;

    function evalPermutation(movePerm) {
        var config = [];
        var permKey = movePerm.map(move => move.state).join() +
            subnodes.map(node => node.id).join();
        if (evaluatedPerms[permKey]) {
            return;
        }
        evaluatedPerms[permKey] = true;
        for (var i = 0; i < movePerm.length; i++) {
            // no need to test config with wrongly assigned seed states
            // TODO there might be neutral target states in the future
            if (movePerm[i].state !== subnodes[i].targetState) {
                return;
            }
            config[subnodes[i].id] = movePerm[i].state;
        }

        callback(config, function() {
            foundPerfect = true;
        });
    }

    function evalSubnodes(_subnodes) {
        subnodes = _subnodes;
        angular$3.forEach(permutator(submoves), evalPermutation);
    }

    function evalSubmoves(_submoves) {
        submoves = _submoves;
        pickN(nodes, submoves.length, evalSubnodes);
    }

    for (var numMoves = 1; numMoves <= moves.length; numMoves++) {
        var possibleMoves = pickN(moves, numMoves);
        angular$3.forEach(possibleMoves, evalSubmoves);
        if (foundPerfect) {
            if (numMoves < moves.length) {
                console.log('found perfect solution with less than all moves: ', numMoves);
            }
            return;
        }
    };
};

const applyConfig = function(levelState, config) {
    angular$3.forEach(config, function(state, node) {
        levelState.setState({id: node}, state);
    });
};

var lastSolvedLevel = null;
var lastSolvedLevelIndex = null;
var lastSolutions = null;

const findSolutions = function(levelIndex) {
    var gameData = window.getng('gameData');
    var level = gameData.data.levels[levelIndex];
    var levelState = window.getng('levelState');
    var wonConfigs = [];
    levelState.initLevel(level);
    levelState.renderer.render(levelState);

    forEachConfig(levelState.moves, level.nodes, function(config, isPerfect) {
        // console.log('config', config);
        levelState.initLevel(level);
        applyConfig(levelState, config);
        levelState.oneStep(levelState.state, 0, false, 0, function() {
            var ts = levelState.checkTargetState();
            // console.log('state', ts);
            // console.log('\n\n');
            if (ts.won) {
                wonConfigs.push({config: config, targetState: ts});
                if (ts.exploded === 0) {
                    isPerfect();
                }
            }
        });
    });
    wonConfigs.sort(function(x, y) {
        return x.targetState.correct - y.targetState.correct;
    });

    lastSolvedLevel = level;
    lastSolutions = wonConfigs;
    lastSolvedLevelIndex = levelIndex;

    console.log(wonConfigs);
    console.log(wonConfigs.length, 'solutions');

    var signature = wonConfigs.map(cf => cf.targetState.correct);
    return signature;
};

const showSolution = function(index) {
    var levelState = window.getng('levelState');
    console.log(lastSolutions);
    levelState.initLevel(lastSolvedLevel, lastSolvedLevelIndex);
    applyConfig(levelState, lastSolutions[index].config);
    levelState.renderer.update(levelState);
    console.log(lastSolutions[index].targetState.correct);
};

// import ActionCtrl from './controllers/ActionCtrl';

const Wad = require('./bower_components/wad/build/wad.js');
const howler = require('./bower_components/howler.js/dist/howler.js'); // TODO use minified
const d3 = require('./bower_components/d3/d3.min.js');
const angular$1 = window.angular;

window.getng = serviceName =>
    angular$1.element(document.body).injector().get(serviceName);
window.findSolutions = findSolutions;
window.showSolution = showSolution;

var app = angular$1

    .module('app', [])

    .factory('jQuery', jQuery)

    .factory('SoundEngine', function () {
        return new SoundEngine(Wad, howler);
    })

    .service('gameData', ['$http', GameData])

    .service('gameState', [
        'gameData', 'SoundEngine',
        (gameData, soundEngine) => new GameState(gameData, soundEngine)])

    .factory('d3', function() {
        return d3;
    })

    .service('networkLogic', [NetworkLogic])

    .factory('options', ['jQuery', 'd3', function($, d3) {
        var d = $('#div_graph');

        var mainColFn = d3.scale.category20();
        var colorFn = function(i) {
            if (i === -1) {
                return '#FF0000';
            }
            return mainColFn(i);
        };

        return {
            renderer: {
                width: d.width(),
                height: d.height(),
                div: d,
                color: colorFn
            }
        };
    }])

    .factory('renderer', ['options', 'jQuery', 'd3', function(options, $, d3) {
        const r = new Renderer(options.renderer, d3);
        const styleEl = document.createElement('style');
        document.head.appendChild(styleEl);
        const styleSheet = styleEl.sheet;
        for (let i = 0; i < 10; i++) {
            styleSheet.insertRule('.state' + i + '{background-color: ' + options.renderer.color(i) + '}', 0);
        }
        $(window).resize(function() {
            r.resize(options.renderer.div.width(), options.renderer.div.height());
        });
        return r;
    }])

    .service('levelState', ['renderer', 'networkLogic', 'SoundEngine', LevelState])

    .controller('MainCtrl', ['$scope', 'gameData', 'gameState', 'SoundEngine', MainCtrl])

    .controller('LevelCtrl', ['$scope', 'gameState', 'levelState', 'renderer', 'SoundEngine', LevelCtrl])

    .controller('GameCtrl', ['$scope', 'gameData', 'gameState',
        ($scope, gameData, gameState) => new GameCtrl($scope, gameData, gameState)])

    .directive('levelselector', function() {
        return {
            restrict: 'E',
            templateUrl: 'templates/levelSelector.html'
        };
    })

    .directive('levelrenderer', function() {
        return {
            restrict: 'E',
            template: '<svg id="svg_graph" style="width:100%;height:100%"></svg>'
        };
    });

module.exports = app;
}());
//# sourceMappingURL=app.js.map