const angular = window.angular;

const pickN = function pickN(a, n, callback) {
    const l = a.length;
    if (n === 0) {
        return [];
    }
    if (n === l) {
        if (callback) {
            callback(a);
        }
        return [a];
    }
    var indices = Array(n);
    for (let i = 0; i < n; i++) {
        indices[i] = i;
    }

    function increase(rindex) {
        const index = n - 1 - rindex;
        indices[index] += 1;
        if (indices[index] === l - rindex) {
            if (index > 0) {
                increase(rindex + 1);
                indices[index] = indices[index - 1] + 1;
            }
        }
    }

    var results = [];
    while (indices[0] <= l - n) {
        var pick = indices.map(function(index) {
            return a[index];
        });
        if (callback) {
            callback(pick);
        } else {
            results.push(pick);
        }
        increase(0);
    }

    return results;
};

const permutator = function permutator(inputArr) {
    var results = [];

    function permute(arr, memo) {
        var cur;
        memo = memo || [];

        for (var i = 0; i < arr.length; i++) {
            cur = arr.splice(i, 1);
            if (arr.length === 0) {
                results.push(memo.concat(cur));
            }
            permute(arr.slice(), memo.concat(cur));
            arr.splice(i, 0, cur[0]);
        }

        return results;
    }

    return permute(inputArr);
};

const forEachConfig = function forEachConfig(moves, nodes, callback) {
    var subnodes = null;
    var submoves;
    var evaluatedPerms = {};
    var foundPerfect = false;

    function evalPermutation(movePerm) {
        var config = [];
        var permKey = movePerm.map(move => move.state).join() +
            subnodes.map(node => node.id).join();
        if (evaluatedPerms[permKey]) {
            return;
        }
        evaluatedPerms[permKey] = true;
        for (var i = 0; i < movePerm.length; i++) {
            // no need to test config with wrongly assigned seed states
            // TODO there might be neutral target states in the future
            if (movePerm[i].state !== subnodes[i].targetState) {
                return;
            }
            config[subnodes[i].id] = movePerm[i].state;
        }

        callback(config, function() {
            foundPerfect = true;
        });
    }

    function evalSubnodes(_subnodes) {
        subnodes = _subnodes;
        angular.forEach(permutator(submoves), evalPermutation);
    }

    function evalSubmoves(_submoves) {
        submoves = _submoves;
        pickN(nodes, submoves.length, evalSubnodes);
    }

    for (var numMoves = 1; numMoves <= moves.length; numMoves++) {
        var possibleMoves = pickN(moves, numMoves);
        angular.forEach(possibleMoves, evalSubmoves);
        if (foundPerfect) {
            if (numMoves < moves.length) {
                console.log('found perfect solution with less than all moves: ', numMoves);
            }
            return;
        }
    };
};

const applyConfig = function(levelState, config) {
    angular.forEach(config, function(state, node) {
        levelState.setState({id: node}, state);
    });
};

var lastSolvedLevel = null;
var lastSolvedLevelIndex = null;
var lastSolutions = null;

export const findSolutions = function(levelIndex) {
    var gameData = window.getng('gameData');
    var level = gameData.data.levels[levelIndex];
    var levelState = window.getng('levelState');
    var wonConfigs = [];
    levelState.initLevel(level);
    levelState.renderer.render(levelState);

    forEachConfig(levelState.moves, level.nodes, function(config, isPerfect) {
        // console.log('config', config);
        levelState.initLevel(level);
        applyConfig(levelState, config);
        levelState.oneStep(levelState.state, 0, false, 0, function() {
            var ts = levelState.checkTargetState();
            // console.log('state', ts);
            // console.log('\n\n');
            if (ts.won) {
                wonConfigs.push({config: config, targetState: ts});
                if (ts.exploded === 0) {
                    isPerfect();
                }
            }
        });
    });
    wonConfigs.sort(function(x, y) {
        return x.targetState.correct - y.targetState.correct;
    });

    lastSolvedLevel = level;
    lastSolutions = wonConfigs;
    lastSolvedLevelIndex = levelIndex;

    console.log(wonConfigs);
    console.log(wonConfigs.length, 'solutions');

    var signature = wonConfigs.map(cf => cf.targetState.correct);
    return signature;
};

export const showSolution = function(index) {
    var levelState = window.getng('levelState');
    console.log(lastSolutions);
    levelState.initLevel(lastSolvedLevel, lastSolvedLevelIndex);
    applyConfig(levelState, lastSolutions[index].config);
    levelState.renderer.update(levelState);
    console.log(lastSolutions[index].targetState.correct);
};
