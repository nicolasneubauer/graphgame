/**
 * Created by nneubaue on 08/11/15.
 */
// overall game state: which levels are visible
class GameState {
    constructor(gameData, soundEngine) {
        console.log(gameData, soundEngine);

        this.activeLevel = null;
        this.levelsState = {};
        this.gameData = gameData;
        this.initialized = false;
        this.soundEngine = soundEngine;
    }

    init () {
        this.levelsState = {};

        for (let i = 0; i < this.gameData.getNumLevels(); i++) {
            this.levelsState[i] = {
                visible: i === 0, //  true, //i == 0,
                score: 0,
                won: false
            };
        }
        this.initialized = true;
    }

    unlockNextLevel(index) {
        const numLevels = this.gameData.getNumLevels();

        if (index >= numLevels - 1) {
            return false;
        }
        if (this.levelsState[index + 1] === undefined) {
            this.levelsState[index + 1] = {};
        }

        this.levelsState[index + 1].visible = true; // TODO make visible to binding thingie
    }
}

export default GameState;
