/**
 * Created by nneubaue on 08/11/15.
 */
// static state which needs to be loaded from JSON
var GameData = function($http) {
    this.data = {
        levels: []
    };
    this.$http = $http;
};

GameData.prototype.loadLevels = function(url, callback) {
    var self = this;
    var $http = self.$http;

    // TODO store in var and to $q.all, rename from loadLevels to loadAll
    $http.get(url).then(function(response) {
        self.data.levels = response.data.levels;
        angular.forEach(self.data.levels, function (level, index) {
            level.id = index;
        });
    });
    callback();
};

GameData.prototype.getNumLevels = function() {
    return this.data.levels.length;
};

export default GameData;
