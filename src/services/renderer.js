var Renderer = function (options, d3) {
    var
        self = this;

    self.options = options;
    self.force = d3.layout.force()
        .charge(-1500)
        .linkDistance(30)
        .size([options.width, options.height]);

    self.svg = d3.select("#svg_graph")
        .attr("width", options.width)
        .attr("height", options.height);

};

Renderer.prototype.resize = function(width, height) {
    var
        self = this;
    self.svg
        .attr("width", width)
        .attr("height", height);

    self.force.size([width, height]);
};

Renderer.prototype.clear = function() {
    "use strict";
    this.render({nodes:[], edges:[]});
};


Renderer.prototype.render = function(levelState) {
    var
        self = this;

    self.svg.selectAll("*").remove();

    self.nodes = levelState.level.nodes.map(function(node) { return JSON.parse(JSON.stringify(node));})
    self.edges = levelState.level.edges.map(function(edge) { return JSON.parse(JSON.stringify(edge));})

    // convert from indices of nodes to references to nodes
    angular.forEach(self.edges, function(edge) {
        edge.source = self.nodes[edge.source];
        edge.target = self.nodes[edge.target];
    })

    self.force
        .nodes(self.nodes)
        .links(self.edges)
        .start();


    self.force.on("tick", function() {
        self.link.attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

        self.node.attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });
    });

    self.needsForceStart = true;
    self.lastNodes = self.nodes.length;

    self.update(levelState);
};

// TODO zoom
// http://bl.ocks.org/mbostock/4699541
// http://jsfiddle.net/56RDx/2/
// http://bl.ocks.org/mbostock/6123708

Renderer.prototype.update = function(levelState) {
    var
        self = this,
        state = levelState.state;



    var showNodes = self.nodes.filter(function (node) {
        // return true;
        if (!state)
            return true;
        return state[node.id] === undefined || state[node.id].state === undefined || state[node.id].state >-1;
    });

    if (showNodes.length != self.lastNodes) {
        self.lastNodes = showNodes.length;
        self.needsForceStart = true;
    }


    var nodeIndices = {};
    for (var i=0; i<showNodes.length; i++) {
        nodeIndices[showNodes[i].id] = i; // mapping old index to new index
    }

    var showEdges = self.edges.filter(function (edge) {
        return (nodeIndices[edge.source.id] !== undefined && nodeIndices[edge.target.id] !== undefined);
    });

    self.link = self.svg.selectAll(".link")
        .data(showEdges);

    self.link.enter().append("line")
        .attr("class", "link")
        .style("stroke-width", 2); // (function(d) { return Math.sqrt(d.value); });

    self.link.exit().remove();



    self.node = self.svg.selectAll(".node")
        .data(showNodes, function(node) { return node.id; });

    self.node
        .enter().append("circle")
        .attr("class", "node")
        .attr("r", 10)
        .style("stroke-width", 5)
        .style("stroke", function(d) { return self.options.color(d.targetState); })
        .call(self.force.drag)
        .on("click", function(d) {
            self.onNodeClick(d);
        })
        .append("title")
        .text(function(d) { return d.name; });

    self.node
        .style("fill", function(d)  {
            if (!state)
                return "#FFF";
            if (!state[d.id])
                return "#FFF";
            if (state[d.id].state === undefined)
                return "#FFF";
            return self.options.color(state[d.id].state);
        }
    );

    self.node.exit().remove();

    if (self.needsForceStart) {
        self.force
            .nodes(showNodes)
            .links(showEdges)
            .start();
        self.needsForceStart = false;
    }
};

Renderer.prototype.onNodeClick = function(o) {
    var self = this;
    if (self._onNodeClick)
        self._onNodeClick(o);
}

Renderer.prototype.setOnNodeClick = function(fn) {
    var self = this;
    self._onNodeClick = fn;
}

export default Renderer
