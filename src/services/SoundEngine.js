const angular = window.angular;
const scales = [
    ['C3', 'D3', 'E3', 'F3', 'G3'],
    ['E3', 'F3', 'G3', 'A3', 'B3'],
    ['G3', 'B3', 'D4', 'C4', 'E4']

];

const makeInstruments = function(Wad) {
    const getNote = (_source, _pitch, _attack, _decay, _sustain, _hold, _release, _delay) =>
        new Wad({
            source: _source,
            pitch: _pitch,
            volume: 0.3,
            env: {
                attack: _attack,
                decay: _decay,
                sustain: _sustain,
                hold: _hold,
                release: _release
            },
            delay: {
                wet: 1,
                delayTime: _delay,
                feedback: 0.25
            },
            tuna: {
                Chorus: {
                    intensity: 0.3,  // 0 to 1
                    rate: 4,         // 0.001 to 8
                    stereoPhase: 0, // 0 to 180
                    bypass: 0
                }
            }
        });

    const niceSound = (note, count, duration, delay) =>
        getNote('sine',
            note, duration * 0.1,
            duration * 0.1,
            duration * 1,
            duration * 0.1,
            duration * 1,
            count > 1 ? duration / count : 0);

    const errorSound = () =>
        getNote('sawtooth', 'F4', 0, 0.1, 0, 0, 0, 0);

    return {
        colors: [
            niceSound,
            niceSound,
            niceSound
        ],
        noMove: errorSound
    };
};

class SoundEngine {
    constructor(Wad, howler) {
        this.scales = scales;
        this.instruments = makeInstruments(Wad);
        this.Wad = Wad;
        this.howler = howler;
    }

    getPureStateDelay(state, duration, numStates) {
        return state * (duration / numStates);
    }

    // TODO know right from wrong moves
    pureStateSound(state, index, count, duration, delay) {
        const instrument = this.instruments.colors[state];
        const note = this.scales[state][index % this.scales[state].length];
        return instrument(note, count, duration, delay);
    };

    playNoMoveSelected () {
        return this.instruments.noMove().play();
    }

    playSingleMove(state, duration) {
        return this.pureStateSound(
            state,
            0,
            1,
            duration / 1000,
            duration / 1000).play();
    };

    /* moves = {node id: [{state: count}]} */
    playMove(moves, index, numStates, duration) {
        const pureStates = {};
        const crashes = {};
        angular.forEach(moves, (states, _nodeId) => {
            if (states.length === 1) {
                var state = states[0].state;
                if (state === undefined) {
                    return;
                }
                if (pureStates[state]) {
                    pureStates[state] += 1;
                } else {
                    pureStates[state] = 1;
                }
            } else {
                const crashIndex = ''; // TODO
                if (crashes[crashIndex]) {
                    crashes[crashIndex] += 1;
                } else {
                    crashes[crashIndex] = 1;
                }
            }
        });

        const poly = new this.Wad.Poly();
        angular.forEach(pureStates, function(count, state) {
            poly.add(this.pureStateSound(
                state,
                index,
                count,
                duration / 1000,
                duration / (1000 * numStates)));
        }, this);
        poly.play();

        // TODO crashSounds
        // TODO distinguish between right and wrong assignment!
        // TODO click sound
    }

    playMusic(occasion) {
        switch (occasion) {
        case 'lounge':
            this.music = new this.howler.Howl({
                src: ['./sound/Groove_Loop.wav'],
                loop: true
            });
            this.music.play();
            break;
        default:
            console.error('unknown occasion ' + occasion);
        };
    }
}

export default SoundEngine;
