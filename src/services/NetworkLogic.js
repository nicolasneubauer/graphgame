/**
 * Created by nneubaue on 15/11/15.
 */
function NetworkLogic() {

}

NetworkLogic.prototype.getNeighbors = function getNeighbors(id, edges, depth) {
    var nodes = {};
    nodes[id] = 0;
    this.extendNodes(nodes, edges, 1, depth);
    return nodes;
};

NetworkLogic.prototype.extendNodes = function(nodes, edges, depth, targetDepth) {
    // TODO: more efficient
    angular.forEach(nodes, function(nodeDepth, node) {
        angular.forEach(edges, function(edge, index) {
            if (edge.target == node) {
                if (!nodes[edge.source])
                    nodes[edge.source] = depth;
            } else if (edge.source == node) {
                if (!nodes[edge.target])
                    nodes[edge.target] = depth;
            }
        });
    });
    if (depth<targetDepth)
        extendNodes(nodes, edges, depth+1, targetDepth);
};



export default NetworkLogic
