/**
 * Created by nneubaue on 08/11/15.
 */
// state of an individual level
var LevelState = function (renderer, networkLogic, soundEngine) {
    this.renderer = renderer;
    this.networkLogic = networkLogic;
    this.soundEngine = soundEngine;
    this.delay = 1500;
};

LevelState.prototype.initLevel = function initLevel(level) {
    this.level = level; // JSON.parse(JSON.stringify(level)); // deep copy, we want to be able to manipulate stuff without worrying about changing the game
    // store index because we will want to remove elements
    angular.forEach(this.level.nodes, function(node, index) {
        node.id = index; // have to call this id because index is overwritten by D3
    });
    angular.forEach(this.level.edges, function(edge, index) {
        edge.id = index;
    });
    this.state = {};
    this.moves = [];
    var states = {};
    var count = 0;
    angular.forEach(level.moves, function(moveType) {
        for(var i=0; i<moveType.count; i++) {
            this.moves.push(moveType);
            moveType.index = count++;
            states[moveType.state] = 1;
        }
    }, this);
    this.possibleStates = Object.keys(states);
    this.numStates = this.possibleStates.length;
};

LevelState.prototype.checkTargetState = function checkTargetState() {
    var
        correct = 0,
        incorrect = 0,
        unset = 0,
        exploded = 0,
        self = this;

    angular.forEach(self.level.nodes, function(node) {
        if (self.state[node.id] === undefined) {
            unset += 1;
        } else if (self.state[node.id].state == -1) {
            exploded += 1;
        } else if (self.state[node.id].state == node.targetState) {
            correct += 1;
        } else {
            incorrect += 1;
        }
    });

    return {
        correct: correct,
        incorrect: incorrect,
        unset: unset,
        exploded: exploded,
        won: incorrect + unset == 0
    }
};


LevelState.prototype.getState = function getState(node) {
    var
        self = this;
    if (!self.state[node.id])
        self.state[node.id] = {};
    return self.state[node.id].state;
};

LevelState.prototype.setState = function setState(node, state) {
    var
        self = this;
    if (!self.state[node.id])
        self.state[node.id] = {};
    self.state[node.id].state = state;
};

LevelState.prototype.oneStep = function oneStep(previousNodes, step, inGame, delay, callback) {
    var
        changedNodes = {},
        numchangedNodes = 0,
        newPrevious = {},
        self = this;

    ///console.log('state', self.state);

    // console.log("previousNodes", previousNodes);

    angular.forEach(previousNodes, function(nodestate, nodeIndex) {
        //console.log('edges', self.level.edges);
        var neighbors = self.networkLogic.getNeighbors(nodeIndex, self.level.edges, 1);
        //console.log("neighbors for node ", nodeIndex, neighbors, ", node has state", nodestate);

        angular.forEach(neighbors, function(depth, nodeIndex2) {
            if (self.state[nodeIndex2]) {
                if (self.state[nodeIndex2].state !== undefined) {
                    // console.log("node has state", node2, self.state[node2]);
                    return;
                }
            }
            if (!changedNodes[nodeIndex2])
                changedNodes[nodeIndex2] = [];
            // TODO more efficient
            if (changedNodes[nodeIndex2].indexOf(nodestate) === -1)
                changedNodes[nodeIndex2].push(nodestate);
            // console.log("node now has state", node2, changedNodes[node2]);
        });
    });

    if (inGame)
        self.soundEngine.playMove(changedNodes, step, this.numStates, delay);

    angular.forEach(changedNodes, function(states, nodeIndex) {
        if (!self.state[nodeIndex])
            self.state[nodeIndex] = {};

        if (states.length == 1) {
            self.state[nodeIndex] = states[0];
            newPrevious[nodeIndex] = states[0];
            numchangedNodes += 1;
        } else {
            var firstState = states[0].state;
            self.state[nodeIndex].state = firstState;
            for (var i=1; i < states.length; i++) {
                if (states[i].state !== firstState) {
                    self.state[nodeIndex].state = -1;
                    break;
                }
            }
        }
    });


    //console.log("vorher", self.state, JSON.stringify(self.level.nodes.map(function(x) { return [x.index, self.state[x.index]];})));
    if (inGame)
        self.renderer.update(self);
    //console.log("nachher", self.level.nodes.map(function(x) { return x.index;}));

    if (numchangedNodes>0) {
        //console.log('changedNodes', numchangedNodes, newPrevious);
        if (inGame)
            setTimeout(function() {self.oneStep(newPrevious, step+1, inGame, delay, callback);}, delay);
        else
            self.oneStep(newPrevious, step+1, inGame, delay, callback);
    }
    else {
        //console.log('final state', self.state);
        callback();
    }
};

LevelState.prototype.play = function play(callback) {
    var self = this;
    this.oneStep(self.state, 0, true, self.delay, callback);
};


export default LevelState
