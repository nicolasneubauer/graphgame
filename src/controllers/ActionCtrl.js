/**
 * Created by nneubaue on 15/11/15.
 */

var ActionCtrl = function($scope) {
    var
        self = this;
    self.$scope = $scope;
};

ActionCtrl.prototype.initLevel = function initLevel(level) {

};

ActionCtrl.prototype.selectedControl = function selectedControl () {

};

ActionCtrl.prototype.onClick = function onClick() {
    var
        self = this;

    self.$scope.$emit('controlSelected', self);
};

/*
function getState(control) {
    var
        classes = control.attr('class'),
        classNames = classes.split(" ");

    for (var i=0; i<classNames.length; i++) {
        var className = classNames[i];
        if (className.substring(0, 5) === 'state')
            return parseInt(className.substring(5));
    }
}
*/

/*

$(".control").click(function(c) {
    var control = $(c.target),
        targetState = getState(control);

    console.log(control, targetState, selectedControl);

    $(".control").removeClass("selectedControl");

    // TODO not just an int, a pointer into the state, so we can decrease the count!
    if (targetState === selectedControl) {
        selectedControl = null; // toggle
    } else {
        control.addClass("selectedControl");
        selectedControl = targetState;
    }
});

*/

export default ActionCtrl;
