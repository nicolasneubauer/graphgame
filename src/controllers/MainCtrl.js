/**
 * Created by nneubaue on 09/11/15.
 */

// main controller, wraps everything
function MainCtrl($scope, gameData, gameState, soundEngine) {
    $scope.name = 'me';
    $scope.ready = false;
    $scope.gameData = gameData;
    $scope.gameState = gameState;
    this.soundEngine = soundEngine;

    gameData.loadLevels('data/levels.json', () => {
        $scope.ready = true;
        this.soundEngine.playMusic('lounge');
    });

    $scope.newGame = () => {
        $scope.gameState.init();
    };

    $scope.showStartScreen = () => !$scope.gameState.initialized;

    $scope.showGameScreen = () => $scope.gameState.initialized;
}

export default MainCtrl;
