/**
 * Created by nneubaue on 09/11/15.
 */
// controls everything for the actual level being played
function LevelCtrl($scope, gameState, levelState, renderer, soundEngine) {
    var self = this;
    self.levelState = levelState;
    self.$scope = $scope;
    $scope.levelState = levelState;
    $scope.won = false;
    $scope.selectedMove = null;
    $scope.selectedMoveIndex = -1;
    self.soundEngine = soundEngine;

    $scope.play = function() {
        self.levelState.play(function() {
            var ts = levelState.checkTargetState();
            console.log('targetState:', ts);
            if (ts.won) {
                self.won();
            }
        })
    };

    this.gameState = gameState;
    this.levelState = levelState;
    this.renderer = renderer;
    $scope.$on('startLevel', function(event, arg) {
        self.initLevel(arg.level, arg.levelIndex);
        $scope.levelState = levelState;
    });
    renderer.setOnNodeClick(function(d) {self.nodeClick(d);}); // save this

    $scope.getMoveClasses = function(move) {
        var s = 'control state' + move.state;
        if (move.index===this.selectedMoveIndex) {
            s += ' selectedMove';
        }
        //'control state'+{{move.state}} + ({{move.index}}=={{selectedMoveIndex}}?' selectedMove':'')
        return s;
    }

    $scope.selectMove = self.selectMove;
}

LevelCtrl.prototype.initLevel = function initLevel(level, levelIndex) {
    var
        self = this;
    self.$scope.won = false;
    self.levelState.initLevel(level);
    self.levelIndex = levelIndex;
    self.$scope.moves = self.levelState.moves;
    self.renderer.render(self.levelState);

};

LevelCtrl.prototype.selectMove = function selectMove(move) {
    if (this.$parent.selectedMove === move) {
        this.$parent.selectedMove = undefined;
        this.$parent.selectedMoveIndex = -1;
    } else {
        this.$parent.selectedMove = move;
        this.$parent.selectedMoveIndex = move.index;
    }
}

LevelCtrl.prototype.won = function won() {
    var self = this;
    self.$scope.$emit("levelWon", self.levelIndex);
}

LevelCtrl.prototype.deleteMove = function deleteMove(move) {
    var moves = this.$scope.moves;
    var index = moves.indexOf(move);
    moves.splice(index, 1);
}

LevelCtrl.prototype.nodeClick = function nodeClick(node) {
    var
        self = this,
        currentState = self.levelState.getState(node);

    console.log(node, currentState);

    if (!self.$scope.selectedMove) {
        console.log("no target state selected!");
        self.soundEngine.playNoMoveSelected();
        return;
    }

    if (currentState !== undefined)
        return false; // state's already set, nothing to do here

    self.levelState.setState(node, self.$scope.selectedMove.state);
    self.soundEngine.playSingleMove(self.$scope.selectedMove.state, 1500); // TODO get 750 from somewhere central


    self.renderer.update(self.levelState);

    self.deleteMove(self.$scope.selectedMove);
    self.$scope.selectedMove = undefined;
    self.$scope.selectedMoveIndex = -1;


    var ts = self.levelState.checkTargetState();

    if (ts.won) {
        self.won();
    }

    self.$scope.$apply();

    return true;
};

export default LevelCtrl;
