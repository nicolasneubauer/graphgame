/**
 * Created by nneubaue on 09/11/15.
 */
// controller for UI above single level, i.e. which levels are visible etc

class GameCtrl {
    constructor ($scope, gameData, gameState) {
        this.$scope = $scope;
        $scope.gameState = gameState;
        $scope.levelsState = gameState.levelsState;

        $scope.runLevel = levelIndex => {
            $scope.gameState.activeLevelIndex = levelIndex;
            $scope.gameState.activeLevel = gameData.data.levels[levelIndex];
            $scope.$broadcast('startLevel', {
                level: $scope.gameState.activeLevel,
                levelIndex: levelIndex
            });
        };

        $scope.getNumLevels = () => gameData.getNumLevels();

        $scope.$on('levelWon', (event, levelIndex) => {
            this.levelWon(levelIndex);
        });
    }

    levelWon (levelIndex) {
        this.$scope.won = true;
        this.$scope.gameState.unlockNextLevel(levelIndex);
        this.$scope.$apply();
    }
}

export default GameCtrl;
