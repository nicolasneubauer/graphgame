'use strict';

import jQuery from './services/jQuery';
import Renderer from './services/renderer';
import GameData from './services/gameData';
import GameState from './services/gameState';
import LevelState from './services/levelState';
import NetworkLogic from './services/NetworkLogic';
import SoundEngine from './services/SoundEngine';

import MainCtrl from './controllers/MainCtrl';
import GameCtrl from './controllers/GameCtrl';
import LevelCtrl from './controllers/LevelCtrl';
// import ActionCtrl from './controllers/ActionCtrl';

import {findSolutions, showSolution} from './helpers/solve.js';

const Wad = require('./bower_components/wad/build/wad.js');
const howler = require('./bower_components/howler.js/dist/howler.js'); // TODO use minified
const d3 = require('./bower_components/d3/d3.min.js');
const angular = window.angular;

window.getng = serviceName =>
    angular.element(document.body).injector().get(serviceName);
window.findSolutions = findSolutions;
window.showSolution = showSolution;

var app = angular

    .module('app', [])

    .factory('jQuery', jQuery)

    .factory('SoundEngine', function () {
        return new SoundEngine(Wad, howler);
    })

    .service('gameData', ['$http', GameData])

    .service('gameState', [
        'gameData', 'SoundEngine',
        (gameData, soundEngine) => new GameState(gameData, soundEngine)])

    .factory('d3', function() {
        return d3;
    })

    .service('networkLogic', [NetworkLogic])

    .factory('options', ['jQuery', 'd3', function($, d3) {
        var d = $('#div_graph');

        var mainColFn = d3.scale.category20();
        var colorFn = function(i) {
            if (i === -1) {
                return '#FF0000';
            }
            return mainColFn(i);
        };

        return {
            renderer: {
                width: d.width(),
                height: d.height(),
                div: d,
                color: colorFn
            }
        };
    }])

    .factory('renderer', ['options', 'jQuery', 'd3', function(options, $, d3) {
        const r = new Renderer(options.renderer, d3);
        const styleEl = document.createElement('style');
        document.head.appendChild(styleEl);
        const styleSheet = styleEl.sheet;
        for (let i = 0; i < 10; i++) {
            styleSheet.insertRule('.state' + i + '{background-color: ' + options.renderer.color(i) + '}', 0);
        }
        $(window).resize(function() {
            r.resize(options.renderer.div.width(), options.renderer.div.height());
        });
        return r;
    }])

    .service('levelState', ['renderer', 'networkLogic', 'SoundEngine', LevelState])

    .controller('MainCtrl', ['$scope', 'gameData', 'gameState', 'SoundEngine', MainCtrl])

    .controller('LevelCtrl', ['$scope', 'gameState', 'levelState', 'renderer', 'SoundEngine', LevelCtrl])

    .controller('GameCtrl', ['$scope', 'gameData', 'gameState',
        ($scope, gameData, gameState) => new GameCtrl($scope, gameData, gameState)])

    .directive('levelselector', function() {
        return {
            restrict: 'E',
            templateUrl: 'templates/levelSelector.html'
        };
    })

    .directive('levelrenderer', function() {
        return {
            restrict: 'E',
            template: '<svg id="svg_graph" style="width:100%;height:100%"></svg>'
        };
    });

export default app;
